## Установка
Установите Ansible. [Тык](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## Конфигурация

**Настройка Ansible**: `cp ansible.cfg.example ansible.cfg` - скопировать файл с примером настроек Ansible. В скопированный файл нужно прописать *inventory* - путь до файла *hosts*

**Настройка hosts**: создайте файл *hosts*, где будет находится информация о ваших хостах (например, наименование и ip адреса)

**Настройка playbook.yaml**: в файле укажите в переменной *new_user* имя пользователя, а в *ssh_pub_key* путь до публичного ключа

## Запуск
`ansible-playbook playbook.yaml`
