#!/bin/bash

root_dir="$1"

for user in $(awk -F':' '{print $1}' /etc/passwd); do
    user_dir="${root_dir}/${user}"
    if [ ! -d "${user_dir}" ]; then
        mkdir "${user_dir}"
        chmod 755 "${user_dir}"
        chown "${user}" "${user_dir}"
        echo "Directory ${user_dir} created successfully"
    else
        echo "Directory ${user_dir} already exists"
    fi
done | tee -a create_directories.log
